import React from 'react';

import FlatlistPets from '../components/Flatlist/Pets'

function Home() {
    return (
        <FlatlistPets />
    )
}

export default Home
